var map = L.map('main_map').setView([4.7490318,-74.0364935,17.5], 30);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    style: 'mapbox://styles/mapbox/light-v10',
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiYWphbHZhcmV6ODkiLCJhIjoiY2toOXRsejZnMDBlZzM0bG1ldHR6dTh5cCJ9.k1MgKQFWgZ1eFqMSKgZn2g'
}).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result)
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        })
    }
})